# Lambda/Anonymous functions

minus = lambda x, y: x - y

# Above lambda function is same as
# def minus(x, y):

# def minus(x, y):
#     return x - y

print(minus(10, 5))


# def a_first(a):
#     return a[1]


a = [[1, 14], [5, 6], [8, 23]]
# a.sort(key=a_first)
a.sort(key=lambda x: x[1])  # instead of def function we can use lambda function
print(a)
