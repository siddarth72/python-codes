a = 9
b = 18
c = sum(a + b)  # bultin function
print(c)


# User defined functions


def printer():
    print("Hello")


printer()


def avg(a, b):
    return (a + b) / 2


print(avg(5, 10))

"""
If there is any return value,
it will be printed in the next line
"""

# DocString


def avgNums(a, b):
    """This function will return the average of two numbers"""
    return (a + b) / 2


print(avgNums.__doc__)
