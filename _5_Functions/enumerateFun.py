l1 = [
    "ladiesfing",
    "potatoes",
    "cucumber",
    "tomatoes",
]

# i = 1
# for item in l1:
#     if i % 2 is not 0:
#         print(f"Please buy this {item}")
#     i += 1

# Above task can be done using enumerate function

# Enumerate
for index, item in enumerate(l1): 
	# enumerate returns both the index and the item
    if index % 2 == 0:
        print(f"Please buy this {item}")
