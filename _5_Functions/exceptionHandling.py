num1 = int(input("Enter a number: "))
num2 = int(input("Enter another number: "))
print(num1 + num2)
"""
If we give any input other than integer, then it will give an error
If there is an error then the below code will not execute
"""
print("This line is very imp")

# Try-except block

print("Enter num1: ")
num1 = input()
print("Enter num2: ")
num2 = input()

try:
    print(int(num1) + int(num2))
except Exception as e:
    print(e)

print("This line is very imp")

# Try-except-else-Finally block

f1 = open("lambda.py")

try:
    f = open("dne.txt")
except Exception as e:
    print(e)
# There can be multiple except blocks for different exceptions
else:
    # This will execute if there is no error
    print("File opened successfully")
    print(f.read())
    f.close()
finally:
    print("Run this anyway")
    f1.close()
    # Finally block will always run

print("Important line")
