numbers = ["3", "2", "1"]

# for i in range(len(numbers)):
#     numbers[i] = int(numbers[i])

# print(numbers[2] + 1)

# Map function
# This above can be done by using Map function

numbers = list(map(int, numbers))
print(numbers[2] + 1)

num = [2, 3, 4, 5, 6, 7]

sq = list(map(lambda x: x ** 2, num))
print(sq)


def sqaure(a):
    return a ** 2


def cube(a):
    return a ** 3


func = [sqaure, cube]

for i in range(5):
    value = list(map(lambda x: x(i), func))
    print(value)

# Filter function

lst1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def is_greater_5(num):
    return num > 5


gt_t5 = list(filter(is_greater_5, lst1))
print(gt_t5)

# Reduce function

from functools import reduce

list1 = [1, 2, 3, 4, 5]

# num = 0
# for i in list1:
#     num += i

# print(num)

sums = reduce(lambda x, y: x + y, list1)
print(sums)
prod = reduce(lambda x, y: x * y, list1)
print(prod)
