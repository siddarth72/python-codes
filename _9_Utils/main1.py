def printer(str):
    return f"This string is {str}"


def add(n1, n2):
    return n1 + n2 + 5


print(__name__)
# This will print as "__main__" if we run this file directly

if __name__ == "__main__":
    print(printer("Hello"))
    x = add(1, 2)
    print(x)
