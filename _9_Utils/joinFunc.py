list = ["Siddarth", "Mahesh", "Prabhat", "Rajesh", "Raju"]

# for item in list:
#     print(item, "and", end=" ")

# This above task can be done using join function

a = " and ".join(list)
print(a, "Others")
