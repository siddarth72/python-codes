# # def func1():
# #     print("func1")


# # func2 = func1
# # del func1

# # func2()


# def funcRet(num):
#     if num == 0:
#         return print
#     if num == 1:
#         return int


# # We can return a function from a function


# a = funcRet(0)
# b = funcRet(1)
# print(a)
# print(b)


# def executor(func):
#     func("this")
# # We can pass a function as an argument to another function


# executor(print)

# Decorator is a function that takes another function as an argument and returns a function


def dec1(func1):
    def nowexec():
        print("Executing now")
        func1()
        print("Executed now")

    return nowexec


@dec1
def whoAmI():
    print("I am a function")


# whoAmI = dec1(whoAmI)
whoAmI()
