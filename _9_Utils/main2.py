import main1

print(main1.add(5, 3))
# This will not only call those functions from main1,
# but also executes the other runnings functions in them

# If we keep all those executions in main thenthose will noit get executed here

# print(__name__)
# This will not print as "__main__", because we are not running the file directly
# in which we have declared that __name__ = "main"
