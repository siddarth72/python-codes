def name_finder():
    import time

    book = open("assignments/co_rout.txt", "rt")
	# Display the contents of the file
    print(book.read())
    time.sleep(4)

    while True:
        text = yield
        if text in book:
            print("Found")
        else:
            print("Not found")


while "y":
    search = name_finder()
    next(search)
    txt = input("Enter your name: ")
    search.send(txt)
    print("\n")
    ans = input("Do you want to continue? (y/n) ")
    if ans == "n":
        break
    else:
        continue
