oxfDict = {
    "Python": "Guido van Rossum",
    "Java": "James Gosling",
    "C++": "Bjarne Stroustrup",
    "C": "Dennis Ritchie",
    "JavaScript": "Brendan Eich",
    "PHP": "Rasmus Lerdorf",
    "Perl": "Larry Wall",
    "Ruby": "Yukihiro Matsumoto",
    "Go": "Robert Griesemer",
    "Swift": "Chris Lattner",
    "Kotlin": "JetBrains",
    "Scala": "Martin Odersky",
    "Rust": "Graydon Hoare",
}

tpl = ()
for key in oxfDict.items():
    tpl += key


print("You can find the meanings of the following programming languages:")
for i in range(len(tpl)):
    print(tpl[i])
print("\n")

# Take input keyword from the user and validate it against the dictionary

keyWord = input("Enter the keyword: ")

if keyWord in oxfDict:
    print(oxfDict[keyWord])
else:
    print("Not found")
