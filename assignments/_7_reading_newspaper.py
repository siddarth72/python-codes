import requests
import json


def speaker(str):
	from win32com.client import Dispatch
	speak = Dispatch("SAPI.SpVoice")
	speak.Speak(str)


r = requests.get("https://newsapi.org/v2/top-headlines?country=us&apiKey=a8e599ac13ce4114914c17e91b4aba80")

data = r.text
parsed = json.loads(data)

print(parsed["articles"][0]["title"])
print(parsed["articles"][0]["description"])

news = {
	"status": parsed["status"],
	"title": parsed["articles"][0]["title"],
	"description": parsed["articles"][0]["description"],
}

speaker(news["description"])
