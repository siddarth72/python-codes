# Function to guess a number form user

n = 72
i = 5

while i:
    i -= 1
    guess = int(input("Guess a number: "))
    if guess == n:
        print("You guessed it!")
        break
    elif guess > n:
        print("Too high!")
    else:
        print("Too low!")
