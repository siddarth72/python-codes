import random

list = ["snake", "water", "gun"]

user = 0
comp = 0

i = 10

while i:
    userDraw = input("Enter your choice: ")
    compDraw = random.choice(list)
    if userDraw == "snake":
        if compDraw == "water":
            print("You win!")
            user += 1
        elif compDraw == "gun":
            print("You lose!")
            comp += 1
        else:
            print("Draw!")
    elif userDraw == "water":
        if compDraw == "gun":
            print("You win!")
            user += 1
        elif compDraw == "snake":
            print("You lose!")
            comp += 1
        else:
            print("Draw!")
    elif userDraw == "gun":
        if compDraw == "snake":
            print("You win!")
            user += 1
        elif compDraw == "water":
            print("You lose!")
            comp += 1
        else:
            print("Draw!")
    else:
        print("Invalid input!")
    i -= 1

if user == comp:
    print("It's a draw!")
elif user > comp:
    print("You win!")
else:
    print("You lose!")
