"""
Water - Drank - every 40mins
Eyes - EyDone - every 30mins
Physical Activity - ExDone - every 45mins
"""

from pygame import mixer
from datetime import datetime
from time import time


def musiconloop(file, stopper):
    mixer.init()
    mixer.music.load(file)
    mixer.music.play()
    while True:
        a = input()
        if a == stopper:
            mixer.music.stop()
            break


def lock_now(msg):
    with open("assignments/mylogs.txt", "a") as f:
        f.write(f"{msg} {datetime.now()} - Locked\n")


if __name__ == "__main__":
    # musiconloop("water.mp3", "stop")
    init_water = time()
    init_eyes = time()
    init_exercise = time()
    water_time = 5
    ex_time = 10
    ey_time = 20

    while True:
        if time() - init_water > water_time:
            print("Water Drinking time")
            musiconloop("assignments/water.mp3", "drank")
            init_water = time()
            lock_now("Drank water at: ")
