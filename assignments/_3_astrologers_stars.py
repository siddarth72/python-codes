"""
Task:-
take input n
take a boolean value

if true:
print:
*
**
***
****

if false:
print:
****
***
**
*
"""

username = "python"
password = "programming"

n = int(input("Enter a number: "))
b_val = True


def print_Asc(n):
    for i in range(1, n + 1):
        print("*" * i)


def print_Desc(n):
    for i in range(n, 0, -1):
        print("*" * i)


def validate_user():
    userName = input("Enter your username: ")
    passWord = input("Enter your password: ")
    if userName == username and passWord == password:
        print("Authenticated successfully")
        b_val = True
    else:
        print("Authentication failed")
        b_val = False
    return b_val


b_val = validate_user()

if b_val:
    print_Asc(n)
else:
    print_Desc(n)
