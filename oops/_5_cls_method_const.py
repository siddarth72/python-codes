# Class methods can be used as alternative constructors


class Employee:
    no_of_leaves = 8

    def __init__(self, name, salary, role):
        self.salary = salary
        self.role = role

    def printDetails(self):
        return f"Name is {self.name}. Salary is {self.salary}. Role is {self.role}"

    @classmethod  # This can only have access to class variables
    def change_leaves(
        cls, new_leaves
    ):  # takes class as input and not self/instance/object
        cls.no_of_leaves = new_leaves

    @classmethod
    def from_str(cls, string):
	    return cls(*string.split("-"))
        # params = string.split("-")  # returns the list of strings seperated by "-"
        # return cls(params[0], params[1], params[2])


siddarth = Employee(
    "Siddarth", 100000, "Programmer"
)  # These all will be handled by the __init__ method
mahesh = Employee("Mahesh", 200000, "Software Engineer")

prabhat = Employee.from_str("Prabhat-250000-uiuxdev")

# siddarth.no_of_leaves = 10  # This will only crate new instance of the object but doesn't changes the Original class variable
# Employee.no_of_leaves = 20  # This will change the class variable

siddarth.change_leaves(10)

print(prabhat.salary)
