"""
Public
Private
Protected - Accessed by those classes that inherit from the base class
"""

class Employee:
    no_of_leaves = 8
	# Protected variables start with _ (naming convention)
    _protec = 72

	# Private variables start with __ (naming convention)
    __priv = 36

    def __init__(self, name, salary, role):
        self.name = name
        self.salary = salary
        self.role = role

    def printDetails(self):
        return f"Name is {self.name}. Salary is {self.salary}. Role is {self.role}"

    @classmethod
    def change_leaves(cls, new_leaves):
        cls.no_of_leaves = new_leaves

    @classmethod
    def from_str(cls, string):
        return cls(*string.split("-"))

    @staticmethod
    def printgood(string):
        print("This is good " + string)


class Programmer():
	pass


emp = Employee("John", 1000, "Student")
print(emp._protec)
# print(emp.__priv) # This will give error as python does not allow to access private variables
print(emp._Employee__priv) # Name mangling

pro = Programmer()
print(pro._protec)
# This will give error as python does not allow to access protected variables to those classes who are not inherited from the base class

