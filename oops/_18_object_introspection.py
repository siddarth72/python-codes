class Employee:
	def __init__(self, fname, lname):
		self.fname = fname
		self.lname = lname

	def explain(self):
		return f"This employee is {self.fname} {self.lname}"

	@property
	def email(self):
		if self.fname == None or self.lname == None:
			return "Email is not set."
		return f"{self.fname}.{self.lname}@gmail.com"

	@email.setter
	def email(self, string):
		print("Setting now...\n\n")
		names = string.split("@")[0]
		self.fname = names.split(".")[0]
		self.lname = names.split(".")[1]


	@email.deleter
	def email(self):
		print("Deleting now...\n\n")
		self.fname = None
		self.lname = None

# Object Introspection: To know about the object and its class
# Every object has an unique id

skillf = Employee("Skill", "F")
print(skillf.email)

o = "This is a string"
print(dir(o))
print(dir(skillf))
# dir function will give us all the attributes and methods of the object/object's class as a list

import inspect

print(inspect.getmembers(skillf))
