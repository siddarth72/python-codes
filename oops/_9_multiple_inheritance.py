class Employee:
    no_of_leaves = 8
    var = 8

    def __init__(self, name, salary, role):
        self.name = name
        self.salary = salary
        self.role = role

    def printDetails(self):
        return f"Name is {self.name}. Salary is {self.salary}. Role is {self.role}"

    @classmethod
    def change_leaves(cls, new_leaves):
        cls.no_of_leaves = new_leaves

    @classmethod
    def from_str(cls, string):
        return cls(*string.split("-"))

    @staticmethod
    def printgood(string):
        print("This is good " + string)


class Player:
    no_of_games = 4
    var = 9

    def __init__(self, name, game):
        self.name = name
        self.game = game

    def printDetails(self):
        return f"Name is {self.name}. Game is {self.game}"


class CoolProgrammer(Employee, Player): # Order of inheritance matters
	language = "C++"

	def printLanguage(self):
		return f"{self.name} codes in {self.language}"


siddarth = Employee("Siddarth", 150000, "Programmer")
mahesh = Employee("Mahesh", 200000, "Software Engineer")

prabhat = Player("Prabhat", "Cricket")

# While initializing the object, arguments must be given according to the order of the class inherited
monesh = CoolProgrammer("Monesh", 450000, "CoolProgrammer")
det = monesh.printDetails()
print(det)
print(monesh.printLanguage())

# abc = CoolProgrammer("Mahesh", "football")
# print(abc.printDetails()) # This will give error as the arguments are not given in the order of the class inherited

print(monesh.var) # This is inherited from Employee because of the order of inheritance

