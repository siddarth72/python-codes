class A:
	classVar1 = "I am a class variable in class A"

	def __init__(self):
		self.var1 = "I am inside class A's constructor"
		self.classVar1 = "Instance variable in class A"
		self.spcl = "I am a special variable in class A"


class B(A):
	# Variable Overriding
	classVar2 = "I am in class B"
	classVar1 = "I am in class B"
	# Even if this is classVar1 is overrided, b.classVar1 will still be the same
	# as A.classVar1 is not overrided as instance variable. It is a class variable

	# Method Overriding
	# If we override the whole __init__ method, we can't use __init__ of the parent class
	# We can use super() to call the parent class's __init__ method
	def __init__(self):
		super().__init__()
		self.var1 = "I am inside class B's constructor"
		self.classVar1 = "Instance variable in class B"



a = A()
b = B()

print(b.classVar1)
# Here b.classVar1 will first find any instance variable in class B, if not found,
# it will find class variable in class B.
# If not found, it will find instance variable in class A.
# If not found, it will find class variable in class A.

print(b.var1)
print(b.classVar1)
print(b.spcl)

# If super() is called after the instance variable, it will use the instance variable of the parent class
