"""
Classes - Template
Object - Instance of the class
DRY - Don't Repeat Yourself
"""


class Student:
    pass


siddarth = Student()
mahesh = Student()

siddarth.name = "siddarth"
siddarth.std = 12
siddarth.dept = "aiml"

mahesh.features = ["Frontend", "Backend", "UI/UX"]

print(siddarth.name)
print(siddarth.std)
print(siddarth.dept)

print(mahesh.features)
