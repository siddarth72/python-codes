class Employee:

    # Dunder methods: All the methods starting with double underscore and ending with double underscore
    # Dunder method is also a constructor
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary

    # Operator overloading: dunder methods that overloads the fuctionality of the operators
    def __add__(self, other):
        return self.salary + other.salary

    def __truediv__(self, other):
        return self.salary / other.salary

    def printDetails(self):
        return f"Name: {self.name}, Salary: {self.salary}"

    def __repr__(self):
        return f"Employee('{self.name}', {self.salary})"

    def __str__(self):
        return f"Name: {self.name}, Salary: {self.salary}"


emp1 = Employee('Mahesh', 10000)
emp2 = Employee('Siddarth', 20000)
print(emp1 + emp2)
print(emp1 / emp2)

print(emp1) # Here is prefers using __str__ instead of __repr__
print(repr(emp2)) # To use __repr__ we need to call seperately (iff __str__ function is defined)
