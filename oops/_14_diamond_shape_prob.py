class A:
	def met(self):
		print("This is a methos from class A")

class B(A):
	def met(self):
		print("This is a methos from class B")

class C(A):
	def met(self):
		print("This is a methos from class C")

class D(B, C):
	pass


a = A()
b = B()
c = C()
d = D()

d.met()
"""
d will find met() in class D,
If it doesnt find it its own class then it will find the same from class B, as B is declared first,
then it will find the same from class C, as C is declared second.
If it doesnt find the met() from classes B and C respectively, it will find met() from class A.
"""

# To avoid diamond shape problem, we avoid using multiple inheritance.
