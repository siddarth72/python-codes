class Employee:
    no_of_leaves = 8

    def __init__(self, name, salary, role):
        self.name = name
        self.salary = salary
        self.role = role

    def printDetails(self):
        return f"Name is {self.name}. Salary is {self.salary}. Role is {self.role}"

    @classmethod
    def change_leaves(cls, new_leaves):
        cls.no_of_leaves = new_leaves

    @classmethod
    def from_str(cls, string):
        return cls(*string.split("-"))

    @staticmethod
    def printgood(string):
        print("This is good " + string)


class Programmer(Employee):  # Programmer is a child class of Employee
    def __init__(self, name, salary, role, lang):
        self.name = name
        self.salary = salary
        self.role = role
        self.lang = lang

    def printProgrammerDetails(self):
        return f"Name is {self.name}. Salary is {self.salary}. Role is {self.role}. Language is {self.lang}"


siddarth = Employee("Siddarth", 150000, "Programmer")
mahesh = Employee("Mahesh", 200000, "Software Engineer")

prabhat = Programmer("Prabhat", 250000, "Programmer", ["Python", "Java", "C++"])

print(prabhat.printProgrammerDetails())
