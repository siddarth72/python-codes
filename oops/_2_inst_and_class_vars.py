class Employee:
    no_of_leaves = 8  # Class variable
    pass


siddarth = Employee()
mahesh = Employee()

# Instance variable

siddarth.name = "Siddarth"
siddarth.salary = 100000
siddarth.role = "Programmer"

mahesh.name = "Mahesh"
mahesh.salary = 200000
mahesh.role = "Software Engineer"

# These created variables cannot be found in class

print(siddarth.no_of_leaves)
print(mahesh.no_of_leaves)

Employee.no_of_leaves = 9  # Original no_of_leaves is overridden
print(siddarth.no_of_leaves)

siddarth.no_of_leaves = 10
# This will not work as no_of_leaves is a class variable

print(siddarth.__dict__)
