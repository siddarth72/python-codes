from abc import ABC, abstractmethod

# ABC is a class that is used to define abstract classes.
# Childs of Abstract class must implement all abstract methods.
class Shape(ABC):
	@abstractmethod
	def printArea(self):
		return 0

# We can't create an object of abstract class.

class Rectangle(Shape):
	type = "Rectangle"
	sides = 4

	def __init__(self):
		self.length = 6
		self.breadth = 7

	# When we are inheriting any class,
	# we need to override the abstract method(mandatory)

	def printArea(self):
		return self.length * self.breadth


rect1 = Rectangle()
print(rect1.printArea())

