class Dad:
	basketball = 1


class Son(Dad):
	dance = 1

	def isdance(self):
		return f"I dance {self.dance} times"


class Grandson(Son):
	dance = 6
	
	def isdance(self):
		return f"I dance incredibly {self.dance} times"

# If two functions/variables are same in multiple classes, then the last class will be the one which will be used
# and will override the method of the previos class/parent class

darry = Dad()
larry = Son()
barry = Grandson()

print(barry.isdance())
print(barry.basketball)

