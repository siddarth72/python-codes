class Employee:
    no_of_leaves = 8  # Class variable

    def __init__(self, name, salary, role):
        self.name = name  # The object's paramater will be equal to equal to the parameters passed while creating and alling the object
        self.salary = salary
        self.role = role

    def printDetails(self):
        # self is a reference to the current instance of the class
        return f"Name is {self.name}. Salary is {self.salary}. Role is {self.role}"


siddarth = Employee(
    "Siddarth", 100000, "Programmer"
)  # These all will be handled by the __init__ method
mahesh = Employee("Mahesh", 200000, "Software Engineer")

# siddarth.name = "Siddarth"
# siddarth.salary = 100000
# siddarth.role = "Programmer"

# mahesh.name = "Mahesh"
# mahesh.salary = 200000
# mahesh.role = "Software Engineer"

print(siddarth.printDetails())
print(mahesh.printDetails())
# Even if u do not give any arguments, the object itself if passed as an argument as default argument
# Thats the reason we send "self" as an argument to the method
