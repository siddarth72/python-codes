class Employee:
	def __init__(self, fname, lname):
		self.fname = fname
		self.lname = lname
		# self.email = f"{fname}.{lname}@sid.com"

	def explain(self):
		return f"This employee is {self.fname} {self.lname}"

	@property
	def email(self):
		if self.fname == None or self.lname == None:
			return "Email is not set."
		return f"{self.fname}.{self.lname}@gmail.com"

	@email.setter
	def email(self, string):
		print("Setting now...\n\n")
		names = string.split("@")[0]
		self.fname = names.split(".")[0]
		self.lname = names.split(".")[1]


	@email.deleter
	def email(self):
		print("Deleting now...\n\n")
		self.fname = None
		self.lname = None

mahesh = Employee("Mahesh", "Banagar")

print(mahesh.explain())

# print(mahesh.email)
# mahesh.fname = "Mahi"
# # Mahesh.fname and hence Mahesh.email will not change in this case
# Without using @property decorator
# print(mahesh.email)

print(mahesh.email)
mahesh.fname = "Mahi"
print(mahesh.email)
# This time the values are changed

# mahesh.email = "fullstack.mahesh@backend.com"
# This will give us error as we cannot change the attribute which has property decorator
# For this we need to use setter decorator

mahesh.email = "fullstack.mahesh@backend.com"
print(mahesh.fname)
print(mahesh.lname)
print(mahesh.email)

# del mahesh.email
# This will give us error as we cannot delete the attribute which has property decorator
# For this we need to use deleter decorator

del mahesh.email
print(mahesh.fname)
print(mahesh.lname)
print(mahesh.email)
