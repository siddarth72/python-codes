class Employee:
    no_of_leaves = 8

    def __init__(self, name, salary, role):
        self.salary = salary
        self.role = role

    def printDetails(self):
        return f"Name is {self.name}. Salary is {self.salary}. Role is {self.role}"

    @classmethod
    def change_leaves(cls, new_leaves):
        cls.no_of_leaves = new_leaves

    @classmethod
    def from_str(cls, string):
        return cls(*string.split("-"))

    @staticmethod
    def printgood(string):
        print("This is good " + string)


siddarth = Employee("Siddarth", 150000, "Programmer")
mahesh = Employee("Mahesh", 200000, "Software Engineer")

prabhat = Employee.from_str("Prabhat-250000-uiuxdev")

siddarth.change_leaves(10)

print(prabhat.salary)
