"""
Arithmetic operators
Comparison operators
Assignment operators
Logical operators
Identity operators
Membership operators
Bitwise operators
"""

# Arithmetic operators
print("3 + 6 =", 3 + 6)
print("3 - 6 =", 3 - 6)
print("3 * 6 =", 3 * 6)
print("3 / 6 =", 3 / 6)
print("3 // 6 =", 3 // 6)
print("3 % 6 =", 3 % 6)  # modulo
print("3 ** 6 =", 3 ** 6)  # 3 to the power of 6

# Assignment operators
x = 3
x += 3  # x = x + 3
print("x =", x)
x -= 3  # x = x - 3
print("x =", x)
x *= 3  # x = x * 3
print("x =", x)
x /= 3  # x = x / 3
print("x =", x)
x //= 3  # x = x // 3
print("x =", x)
x %= 3  # x = x % 3
print("x =", x)
x **= 3  # x = x ** 3
print("x =", x)

# Comparison operators
i = 72
print("i =", i)
print(i == 72)
print(i != 72)

# Logical operators
k = True
j = False
print(k and j)  # and operator: True and True = True only
print(k or j)  # or operator: False or False = False only
print(not k)  # not operator: not True = False

# Identity operators
a = True
b = False
print(a is True)  # is operator: a is True = True
print(a is b)  # is operator: a is b = False
print(a is not b)  # is not operator: a is not b = True

# Membership operators
list = [1, 2, 3, 4, 5]
print(1 in list)  # in operator: 1 in list = True
print(6 in list)  # in operator: 6 in list = False
print(1 not in list)  # not in operator: 1 not in list = False

# Bitwise operators
# Playing with binary numbers
"""
&: a.b
|: a+b
^: a^b
~: ~a
<<: a<<b
>>: a>>b
"""
print(0 & 1)
print(0 | 1)

"""
"is" vs "=="
== : Value equality - Two objects having same value
is : Reference equality - Two reference pointing to same object
"""

"""
>>> a = [1, 2, 3]
>>> b = a
>>> b == a
True
>>> b is a
True
>>> b[0] = 0
>>> a
[0, 2, 3]
>>> c = a[:]
>>> c
[0, 2, 3]
>>> a
[0, 2, 3]
>>> c[::-1]
[3, 2, 0]
>>> c is a
False
"""
