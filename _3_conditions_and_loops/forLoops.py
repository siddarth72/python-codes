list1 = [
    ["Siddarth", "Backend"],
    ["Prabhat", "Frontend"],
    ["Mahesh", "Fullstack"],
]

# print(list1[0])
# print(list1[1])
# print(list1[2])
# print(list1[3])
# print(list1[4])

for name, des in list1:
    print(f"{name} is a {des} Developer")


dict1 = dict(list1)
# for name, desg in dict1: # This will not work
for name, desg in dict1.items():
    print(f"{name} is a {desg} Developer")


for item in dict1:  # this will work
    print(item)

myItems = [int, float, "Sidd", 1, 8, 3, 6]

for item in myItems:
    if str(item).isnumeric() and item > 6:
        print(item)
