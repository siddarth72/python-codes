i = 0

while True:
    print(i + 1, end=" ")
    if i == 45:
        break
    i += 1


while True:
    # continue statement
    i += 1
    if i == 45:
        continue
    print(i, end=" ")
