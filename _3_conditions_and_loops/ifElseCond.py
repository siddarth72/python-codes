var2 = 72

var3 = int(input("Enter a number: "))

if var3 > var2:
    print("Greater")
elif var3 == var2:
	print("Equal")
else:
    print("Lesser")

list1 = [5, 7, 3]
print(5 in list1)
print(15 in list1)
if 5 in list1:
	print("5 is in the list")
if 25 not in list1:
	print("25 is not in the list")


age = int(input("Enter your age: "))
if age < 18:
    print("You are not eligible to vote")
elif age == 18:
    print("Make sure you have completed your birth certificate")
else:
    print("You are eligible to vote")

# Short Hand notation

a = int(input("Enter a: "))
b = int(input("Enter b: "))

if a > b:
    print("A is greater")

print("A") if a > b else print("B")
