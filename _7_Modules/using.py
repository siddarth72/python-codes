import random

random_number = random.randint(0, 5)  # generate a random number between 0 and 5
print(random_number)

rand1 = random.random()  # generate a random number between 0 and 1
print(rand1)

rand2 = random.random() * 100  # generate a random number between 0 and 100
print(rand2)

lst = ["stars sports", "dd1", "aaj tak", "sab tv"]
chc = random.choice(lst)  # randomly select an item from a list
print(chc)
