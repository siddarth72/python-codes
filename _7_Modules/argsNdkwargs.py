# def func(a, b, c, d):
#     print(a, b, c, d)


# func("1", "2", "3", "4", "5") # this wont work


# Whenever we pass args to a function, it is called a tuple.
def funArgs(normal, *args, **kwargs):
    print(normal)

    for arg in args:
        print(arg)

    for key, value in kwargs.items():
        print(f"{key} is a {value} developer")


# Always keeps the normal argument first
# then args and then kwargs

# args and kwargs are optional and need not necessarily be present

lst = ["Siddarth", "Mahesh", "Prabhat"]

normal = "This is a normal string"

kw = {"Siddarth": "Backend", "Mahesh": "Full stack", "Prabhat": "Frontend"}

print("Normal:-")
funArgs(normal)
print("With args:-")
funArgs(normal, *lst)
print("With args kwargs:-")
funArgs(normal, *lst, **kw)
