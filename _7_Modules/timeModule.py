import time

initial = time.time()  # time.time() returns the current time in seconds since the epoch
print(initial)

k = 0
while k < 45:
    print("Sidd")
    k += 1

print("While loop ran in", time.time() - initial, "seconds")

for i in range(45):
    print("Sidd")

print("For loop ran in", time.time() - initial, "seconds")

localTime = time.asctime(time.localtime(time.time()))
print(localTime)
