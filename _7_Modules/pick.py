import pickle

devstack = ["Frontend", "Backend", "Database", "Devops", "Testing", "Fullstack"]
# We can store this obj(list) in a file

# Pickling
file = "devstack.pkl"
fileObj = open(file, "wb")
pickle.dump(devstack, fileObj)
fileObj.close()
# The file which is created is a binary file and cannot be read easily

# Unpickling
# Reading the data from the file that we created using pickle
file2 = "devstack.pkl"
fileObj = open(file2, "rb")
devstackList = pickle.load(fileObj)
print(type(devstackList))
print(devstackList)
