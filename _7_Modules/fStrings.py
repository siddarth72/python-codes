# String Formatting
import math

lang = "Python"
typ = "Programming"
# a = "This is %s" % me  # %s is a placeholder for a string
# print(a)
# This is not good practice, because it is hard to read and understand.

# a = "This is {} {}"
# # a = "This is {1} {0}" # {0} is a placeholder for a string
# b = a.format(lang, typ)
# print(b)

# F-Strings

a = f"This is {lang} {typ} {72*3}"
b = f"This is {lang} {typ} {math.cos(75)}"
print(a)
print(b)
