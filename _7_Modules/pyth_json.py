import json

data = '{"name": "John", "age": 30, "city": "New York"}'
print(data)
# Here key value pairs cannot be accessed by index

parsed = json.loads(data)
print(parsed)
# Here key value pairs can be accessed by index

utils = {
	"name": "Siddarth",
	"age": 20,
	"city": "Bangalore",
	"designation": "Deep Learning Engineer",
	"languages": ["Python", "Java", "C++", "JavaScript"],
	"projects": ("Artificial Intelligence", "Machine Learning", "Data Science")
}

jsonified = json.dumps(utils)
print(jsonified)
# Jasonified is exactly the same as JavaScript Object Notation Syntax
