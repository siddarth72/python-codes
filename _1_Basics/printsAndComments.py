# Prints and comments

"""
This is file to demonstrate printing and comments
This is a multi-line comment
"""

print("Python is fun!", end=", ")
print("Do learn python")
print("Next Line")

# Escape sequence

print("Python\nTutorial")
print("Python'nTutorial")

print("Python \t is fun")  # Comment after statement
