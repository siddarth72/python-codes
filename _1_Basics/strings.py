mystr = "Python Programming"
print(mystr[8])
print(len(mystr))

# # String slicing
print(mystr[0:4])  # 0-> including, 4-> excluding
print(mystr[0 : len(mystr)])

print(mystr[:])  # print(mystr[0:len(mystr)])
print(mystr[:9])  # print(mystr[0:len(mystr)])
print(mystr[8:])  # print(mystr[8:0])
print(mystr[0:72])
# print(mystr[72])  # this gives error

print(mystr[::])  # print(mystr[0:len(mystr):1])
print(mystr[0 : len(mystr) : 2])  # gap of 2

# Negative indexing
print(mystr[-4:-2])  # starts pointing from the end
# print(mystr[-4:-2])  # same as print(mystr[13:15])
print(mystr[::-1])  # reverse the string

print(
    mystr.isalnum()
)  # check if string is alphanumeric, is there are spaces then also its not valid
print(mystr.endswith("ing"))  # check if string ends with "ing"
print(mystr.count("p"))  # count the number of "p" in the string
print(mystr.capitalize())  # capitalize the first letter
print(mystr.find("ing"))
print(mystr.replace("Python", "C++"))
