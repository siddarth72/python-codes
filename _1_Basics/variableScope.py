l = 10  # Global variable


def f():
    M = 20  # Local variable,
    # only accessible in this function
    print(M)


# We cannot change the values of global variables directly
# We can change them only by 'global' keyword


def changeGlobal():
    global l
    l += 30
    print(l)


def f1():
    x = 20

    def f2():
        global x  # This is not a global variable,
        # because it is declared in function f1
        x = 81

    print("Before calling f2()", x)
    f2()
    print("After calling f2()", x)


f1()
