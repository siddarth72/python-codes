var1 = "Programming"
var2 = 4
var3 = 5.5
var4 = True
var5 = "Python"

print(type(var1))
print(type(var2))
print(type(var3))
print(type(var4))

# Concatenation
print(var1 + var5)
print(var1 + str(var2))  # converting to string and then concatenate

a = "72"
b = "73"
print(a + b)

"""
str()
int()
float()
bool()
"""

# Type casting
print(int(a) + int(b))

print(5 * "Siddarth\n")
print(5 * str(int(a) + int(b)))

# Inputs

inpNum = input("Enter your number: ")
# inpNum = int(input("Enter your number: "))

# input() returns a string, so it cannot be used for arithmetic operations
print("You entered: ", int(inpNum) + 5)

# Function to add two numbers
n1 = int(input("Enter first number: "))
n2 = int(input("Enter second number: "))
print("Sum: ", n1 + n2)
