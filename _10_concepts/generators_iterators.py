"""
Iterable - __iter__() or __getitem__(), Tells us whether an object can be iterated over or not.
Iterator - __next__()
Iteration
"""

# for i in range(78): # Here range is a generator, which generates a sequence of numbers.
# 	print(i)


def gen(n):
	for i in range(n):
		yield i
		# yield is a keyword, which tells the function to return a generator object.
		# It generates a sequence of numbers one at a time.

# Generator is an iterator which can be run only once at a time.

g = gen(9)
print(g)

print(g.__next__()) # This will print 0
print(g.__next__()) # This will print 1
print(g.__next__()) # This will print 2
print(g.__next__()) # This will print 3
# so on until it reaches the end of the sequence.

s = "siddarth"
# for sid in s:
# 	print(sid)
# Strings can be iterable.
# Strings have __iter__() method.
# print(iter(s)) # This will return an iterator object.
ier = iter(s)
print(ier.__next__()) # This will print s
print(ier.__next__()) # This will print i
print(ier.__next__()) # This will print d
print(ier.__next__()) # This will print a
print(ier.__next__()) # This will print r
print(ier.__next__()) # This will print t
print(ier.__next__()) # This will print h
# print(ier.__next__()) # This will throw an error.

# ints are not iterable.