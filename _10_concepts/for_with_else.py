khana = ["chapati", "rice", "paneer", "dal", "chutney"]

for item in khana:
    print(item)
    break

else:
    print("Khana is over")  # This will not be printed.


# For loop can end in two cases:
# 1. When the iterator is exhausted.
# 2. When there is break statement.

# Else with for loop is valid iff there is no break statement.
