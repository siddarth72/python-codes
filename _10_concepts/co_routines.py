def searcher():
    import time

    # Some 4 seconds time consuming task
    book = "This is the best on operating systems"
    time.sleep(4)

    while True:
        text = yield
        if text in book:
            print("Found")
        else:
            print("Not found")


# We use coroutine in which initially the function/program will take some time to find or read some data or task

search = searcher()
next(search)
search.send("best")

# Initially function will take 4 seconds to start the coroutine
# from second time onwards, the search will be resumed from the yield statement

input("\nPress Enter to continue...")
search.send("operating sys")
