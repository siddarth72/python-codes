a = input("Name: ")
b = input("Earning: ")

if int(b) == 0:
	raise ZeroDivisionError("Earning cannot be zero")

if a.isnumeric():
	raise ValueError("Numbers are not allowed")

print(f"Hello {a}")


c = input()

try:
	print(a)
except Exception as e:
	if c == "Fun":
		raise ValueError("Fun is not allowed")
	print("Exception handled")


try:
	desg = input("Designation: ")
except  EOFError:
	print("EOF, no designation")
except KeyboardInterrupt:
	print("Keyboard Interrupt")
else:
	print('There is some format error')
