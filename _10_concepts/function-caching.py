import time
from functools import lru_cache


@lru_cache(maxsize=3) # Now there is mo more delay from the second time
def some_work(n):
    time.sleep(n)
    return n


if __name__ == "__main__":
    print("Now runnig some work:")
    some_work(3)
    print("Done... calling again")
    some_work(3)
    print("called again")
