# ls = []

# for i in range(100):
# 	if i % 3 == 0:
# 		ls.append(i)
# print(ls) # This will print all the numbers divisible by 3.

# This above task can be done using a comprehension.

# List Comprehension
ls = [i for i in range(20) if i % 3 == 0]
print(ls)

# Dictionary Comprehension
dict1 = {i: f"item{i}" for i in range(20) if i % 3 == 0}
print(dict1)
# Reverse the above dictionary comprehension.
dict2 = {value: key for key, value in dict1.items()}
print(dict2)

# Set Comprehension
dresses = {dress for dress in ["dress1", "dress2", "dress3"]}
print(dresses)

# Generator Comprehension
evens = (i for i in range(20) if i % 2 == 0)
for item in evens:
    print(item)
