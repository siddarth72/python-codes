import os

# Get the current working directory
print(os.getcwd())

# Change the current working directory
os.chdir("C://")
print(os.getcwd())

f = open("_11_os_Module/modOs.txt")

# Get all the files in the current working directory
print(os.listdir())

# Create a new directory
os.mkdir("Utils")
os.mkdir("_11_os_Module/Utils") # This first finds the directory and then creates the directory

# Make multiple directories
os.makedirs("Containers/Components") # This will create two nested directories

# Rename a file/directory
os.rename("_11_os_Module/modOs.txt", "_11_os_Module/os_mod.txt")

# Get all Environment Variables
print(os.environ.get('Path'))

# Check if the path exists
print(os.path.exists("_11_os_Module/modOs.txt")) # This doesn't exist
print(os.path.exists("_11_os_Module/os_mod.txt")) # This does exist

# Check if the path is a file and exists
print(os.path.isfile("_11_os_Module/os_mod.txt")) # This is a File
print(os.path.isfile("_11_os_Module")) # This is a Directory

# Check if the path is a directory and exists
print(os.path.isdir("_11_os_Module/os_mod.txt")) # This is a File
print(os.path.isdir("_11_os_Module")) # This is a Directory


