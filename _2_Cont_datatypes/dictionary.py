# Key value pairs

d1 = {"a": 1, "b": 2, "c": 3}
d2 = {
    "Siddarth": "Backend",
    "Mahesh": "Frontend",
    "Rajesh": {"Backend": "Python", "Frontend": "JavaScript"},
}
print(d1)
print(d2)

print(d2["Siddarth"])
print(d2["Rajesh"])
print(d2["Rajesh"]["Backend"])

d2["Monesh"] = "Fullstack"
print(d2)

del d2["Rajesh"]
print(d2)

d3 = d2
d3["Rajesh"] = "Fullstack"
print(d2)  # even d2 is changed

d3 = d2.copy()
d3["Rajesh"] = "Fullstack"
print(d2)  # even d2 is not changed
print(d3)

d2.update({"Rajesh": "Devops"})
print(d2)
print(d2.items())
print(d2.keys())
print(d2.values())
