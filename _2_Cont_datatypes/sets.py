s = set()
print(type(s))

s_from_list = set([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
print(s_from_list)
print(type(s_from_list))

s.add(1)
print(s)
s.add(1)
print(s)  # Here only unique elements are allowed in the set

s1 = s.union(s_from_list)
print(s1)
s2 = s.intersection(s_from_list)
print(s2)

print(s.isdisjoint(s1))

s.remove(1)
print(s)
