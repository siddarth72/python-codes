"""
Lists are mutable
"""

grocery = [
    "Milk",
    "Bread",
    72,
    "Cheese",
    "Butter",
    36,
]
print(grocery)
print(grocery[0])
print(grocery[1])
print(grocery[2])
print(grocery[3])

numbers = [2, 8, 6, 5, 3]
print(numbers)
numbers.sort()
print(numbers)
numbers.reverse()
print(numbers)

"""
Functions like sort and reverse changes the whole list
while slicing operations keeps the list as it is
"""

print(numbers[0:3])  # print(numbers[:3])
print(numbers[1:4])

print(numbers[::1])
print(numbers[::2])  # skips 1 element
print(numbers[::-1])  # reverse the list
# take -1 only while negative slicing

numbers.append(1)  # add "1" to the end of the list
print(numbers)

numbers.insert(1, 4)  # insert "4" at index 1
print(numbers)

numbers.remove(6)
print(numbers)

numbers.pop()
print(numbers)

numss = [1, 2, 3, 6, 5, 6]
numss.remove(6)
print(numss)
