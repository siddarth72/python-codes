"""
Tuples are immutable
"""

tp = (6, 9, 3)
print(tp)
# Values of tuples cannot be changed
tp2 = (1,)  # this is a tuple

# Swapping
a = 10
b = 20
print(f"a = {a}, b = {b}")
a, b = b, a
print(f"a = {a}, b = {b}")
