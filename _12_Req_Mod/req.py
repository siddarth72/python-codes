import requests

# GET Request
r = requests.get('https://www.baidu.com')
print(r.text)
print(r.status_code)

# POST Request
url = "www.something.com"
data = {'key1': 'value1', 'key2': 'value2'}
r2 = requests.post(url=url, data=data)

