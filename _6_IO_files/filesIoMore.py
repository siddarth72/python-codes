f = open("_6_IO_files/ios.txt")
print(f.tell())  # gives the current position of the cursor
print(f.readline())
f.seek(0)  # resets the file pointer to the beginning of the file
f.seek(10)  # moves the file pointer to the 10th position
print(f.tell())
print(f.readline())
print(f.tell())

f.close()
