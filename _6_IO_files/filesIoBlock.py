# Normal opening of the file

# f = open("_6_IO_files/ios.txt")
# print(f.readline())
# f.close()


# Opening the file using with block

with open("_6_IO_files/ios.txt") as f:
    a = f.read()
    print(a)
# No need to close the file,
# it is closed automatically when the with block ends
