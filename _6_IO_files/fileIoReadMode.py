"""
r -> Open file for reading (default)
w -> Open file for writing
x -> Create file if not exist
a -> Open file for appending
t -> Open file in text mode (default)
b -> Open file in binary mode
+ -> Open file for updating (reading and writing)
"""

f = open("_6_IO_files/ios.txt", "rt")

content = f.read()

for line in content:
    print(line)  # prints character by character

for line in f:
	print(line, end=" ") # prints line by line

print(content)

content = f.read(10)
print(content)

print(f.readline())
print(f.readline())

# Storing each line of the file in a list
print(f.readlines())

f.close()
